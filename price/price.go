package price

import (
	"fmt"
	"time"

	"github.com/Rhymond/go-money"
	"gitlab.com/Kivanc10/sample_app2_test/product"
)

type Item struct {
	product.Product
	Quantity uint8
}

type Prices struct {
	Items        []Item
	CurrencyCode string
	SoldTime     time.Time
}

func (p *Prices) TotalPrice() (*money.Money, error) {
	total := money.New(0, p.CurrencyCode) // initializing with 0
	var err error                         // define value named err that has type error
	for _, v := range p.Items {
		subTotal := v.Product.Price.Multiply(int64(v.Quantity))
		total, err = total.Add(subTotal)
		if err != nil {
			fmt.Println("an error occured")
			return nil, err
		}
	}
	return total, nil
}
