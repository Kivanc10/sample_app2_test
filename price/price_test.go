package price

import (
	"fmt"
	"testing"
	"time"

	"github.com/Rhymond/go-money"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Kivanc10/sample_app2_test/product"
)

func TestPrice(t *testing.T) {
	items := []Item{
		{
			Product: product.Product{
				ID:    "p_1299**",
				Name:  "Product test",
				Price: money.New(1000, "EUR"),
			},
			Quantity: 2,
		},
		{
			Product: product.Product{
				ID:    "p_1300**",
				Name:  "Product test 2",
				Price: money.New(2131, "EUR"),
			},
			Quantity: 1,
		},
		{
			Product: product.Product{
				ID:    "p_1301**",
				Name:  "Product test 3",
				Price: money.New(1400, "EUR"),
			},
			Quantity: 2,
		},
	}

	c := Prices{
		Items:        items,
		CurrencyCode: "EUR",
		SoldTime:     time.Now(),
	}
	//  true answer --> 6931
	expected := money.New(6930, c.CurrencyCode)

	actual, err := c.TotalPrice()

	if err != nil {
		fmt.Println("an error occured v2-")
		return
	}

	assert.Equal(t, expected, actual)

}
