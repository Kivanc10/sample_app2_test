package getString

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
the compiler ignored this file
this file is the unit test
the tool named assert is belong to an api located in github.com/stretchr/testify/assert, it allows us to
check our functions,methods are work as expected.

assert func ---> first parameter is ---> expected(developer expected)
assert func ---> second parameter is the func I want to test(to get actual response)
assert func ---> last parameter return an expression when the system throw an error

this func will throw an verbose error becasue expected value and actual value are different
*/
func TestGetString(t *testing.T) {
	assert.Equal(t, "this is my string !!", GetString(), "those responses should be equal")
}
